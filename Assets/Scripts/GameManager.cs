﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private float TargetSeconds;
    private int LastTickSecond;
    public static GameManager manager;
    public CollectablePriority.rankOfPriority TargetPriority { get; private set; }

    private Dictionary<CollectablePriority.rankOfPriority, string> objectives = new Dictionary<CollectablePriority.rankOfPriority, string>();

    public Transform first;
    public Transform second;
    public Transform third;
    public Transform fourth;
    public Transform fifth;

    public List<GameObject> collectedList;

    public Transform Clock;
    public Transform NextObjective;
    public Transform suitcase;
    private SoundPlayer SoundPlayer;
    //public Transform Player;

    public bool reload;

    private void Awake()
    {
        SoundPlayer = GetComponent<SoundPlayer>();
        manager = this;
        Physics.IgnoreLayerCollision(2, 10);
        TargetSeconds = 90;
        LastTickSecond = (int)TargetSeconds;
        reload = false;

        objectives[CollectablePriority.rankOfPriority.First] = "Candle";
        objectives[CollectablePriority.rankOfPriority.Second] = "Detergent Bottle";
        objectives[CollectablePriority.rankOfPriority.Third] = "Perfume";
        objectives[CollectablePriority.rankOfPriority.Fourth] = "Muffin";
        objectives[CollectablePriority.rankOfPriority.Fifth] = "Train";
        objectives[CollectablePriority.rankOfPriority.DONE] = "YOU WIN";
    }

    private void Start()
    {
        SoundPlayer.PlayLoop(SoundPlayer.SoundTrack.Track1);
    }

    private void Update()
    {
        if (reload)
        {
            TargetPriority = CollectablePriority.rankOfPriority.First;
            StartCoroutine(ReloadWithDelay(3));
        }
        else
        {
            if (TargetSeconds <= 0)
            {
                NextObjective.GetComponent<TextMeshPro>().text = "YOU LOSE";
                SoundPlayer.StopTrackPlayer();
                SoundPlayer.PlayEffect(SoundPlayer.SFX.Fail);
                reload = true;
            }

            if (TargetPriority == CollectablePriority.rankOfPriority.DONE)
            {
                NextObjective.GetComponent<TextMeshPro>().text = "YOU WIN";
                SoundPlayer.StopTrackPlayer();
                SoundPlayer.PlayEffect(SoundPlayer.SFX.Win);
                reload = true;
            }
            else
            {
                TargetSeconds -= Time.deltaTime;
                Clock.GetComponent<TextMeshPro>().text = ((int)TargetSeconds).ToString();

                if (LastTickSecond <= 10 && LastTickSecond != (int)TargetSeconds)
                {
                    SoundPlayer.PlayEffect(SoundPlayer.SFX.ClockTick);
                }
                LastTickSecond = (int)TargetSeconds;
            }
        }
    }

    public bool CollectItem(GameObject CollectedItem)
    {
        // NOTE(batuhan): This will "boom" trash items
        bool IsBadItem = false;
        collectedList.Add(CollectedItem);
        var Collectable = CollectedItem.GetComponent<CollectablePriority>();

        if (Collectable?.Type == CollectableBase.CollectableType.RealItem)
        {
            if (Collectable.priority != TargetPriority)
            {
                IsBadItem = true;
            }
        }
        else
        {
            IsBadItem = true;
        }

        if (IsBadItem)
        {
            TargetPriority = CollectablePriority.rankOfPriority.First;
            NextObjective.GetComponent<TextMeshPro>().text = "Next objective: " + objectives[TargetPriority];
            //CollectedItem.Boom();
        }
        else
        {
            TargetPriority = (CollectablePriority.rankOfPriority)(((int)TargetPriority) + 1);
            NextObjective.GetComponent<TextMeshPro>().text = "Next objective: " + objectives[TargetPriority];
            Collectable?.PutInBox();
            SoundPlayer.PlayEffect(SoundPlayer.SFX.Collect);
        }

        return !IsBadItem;
    }

    IEnumerator ReloadWithDelay(float Delay)
    {
        yield return new WaitForSeconds(Delay);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
