﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{   
    public enum SFX
    {
        Effect1,
        Effect2,
        Effect3,
        Effect4,
        Effect5,
        Effect6,
        ClockTick,
        Win,
        Fail,
        Collect
    };

    public enum SoundTrack
    {
        Track1,
        Track2,
        Track3
    };

    [Serializable]
    public struct inspector_sfx
    {
        public SFX Effect;
        public AudioClip Clip;
    }

    [Serializable]
    public struct inspector_track
    {
        public SoundTrack Track;
        public AudioClip Clip;
    }

    public inspector_sfx[] SFXClips;
    public inspector_track[] TrackClips;

    public AudioSource EffectPlayer;
    public AudioSource TrackPlayer;

    private Dictionary<SFX, AudioClip> SFXLibrary = new Dictionary<SFX, AudioClip>();
    private Dictionary<SoundTrack, AudioClip> TrackLibrary = new Dictionary<SoundTrack, AudioClip>();
    private int QueueIndex = 0;

    public void Awake()
    {
        foreach (var clip in SFXClips)
        {
            SFXLibrary.Add(clip.Effect, clip.Clip);
        }

        foreach (var track in TrackClips)
        {
            TrackLibrary.Add(track.Track, track.Clip);
        }
    }

    public void PlayEffect(SFX Effect)
    {
        EffectPlayer.PlayOneShot(SFXLibrary[Effect]);
    }

    public void PlayLoop(SoundTrack Track)
    {
        StopTrackPlayer();
        TrackPlayer.clip = TrackLibrary[Track];
        TrackPlayer.loop = true;
        TrackPlayer.Play();
    }

    public void StopTrackPlayer()
    {
        if (TrackPlayer.isPlaying)
        { 
            TrackPlayer.Stop();
        }
    }
}
