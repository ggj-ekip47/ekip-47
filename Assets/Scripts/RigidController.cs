﻿using System;
using UnityEngine;

public class RigidController : MonoBehaviour
{
    //Assingables
    public Transform camera;
    public Transform head;
    public float Force = 2500.0f;
    public float Gravity = 9.81f;

    private Rigidbody m_RigidBody;
    private Quaternion m_CurrentOrientation;

    //Rotation and look
    private float xRotation;
    private float sensitivity = 50f;
    private float sensMultiplier = 1f;

    //Movement
    public float MaxSpeed = 10;
    public bool grounded;
    public LayerMask Solid;

    public float counterMovement = 1.35f;
    private float threshold = 0.01f;
    public float maxSlopeAngle = 35f;

    //Input
    private Vector3 m_MovementDirection = new Vector3();
    private Vector3 m_LookDirection = new Vector3();

    //Sliding
    private Vector3 normalVector = Vector3.up;
    private Vector3 wallNormalVector;

    void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody>();
    }

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        // gather inputs
        m_MovementDirection.x = Input.GetAxisRaw("Horizontal");
        m_MovementDirection.y = Input.GetAxisRaw("Vertical");
        m_MovementDirection.z = 0.0f;

        // x-y flipped to ease the math
        m_LookDirection.x = Input.GetAxis("Mouse Y");
        m_LookDirection.y = -Input.GetAxis("Mouse X");
        m_LookDirection.z = 0.0f;

        // apply options
        m_LookDirection *= sensitivity * Time.fixedDeltaTime * sensMultiplier;

        Vector3 CameraRotation = camera.transform.localRotation.eulerAngles;
        CameraRotation -= m_LookDirection;

        // snap the camera to the head & rotate
        camera.transform.position = head.transform.position;
        camera.transform.localRotation = Quaternion.Euler(CameraRotation.x, CameraRotation.y, 0.0f);
        m_CurrentOrientation = Quaternion.Euler(0.0f, CameraRotation.y, 0.0f);
    }

    void FixedUpdate()
    {
        Vector2 CurrentVelocity = FindRelativeVelocity();
        if (grounded)
        {
            if (Math.Abs(CurrentVelocity.x) > threshold && Math.Abs(m_MovementDirection.x) < 0.05f ||
                (CurrentVelocity.x < -threshold && m_MovementDirection.x > 0) ||
                (CurrentVelocity.x > threshold && m_MovementDirection.x < 0))
            {
                m_RigidBody.AddForce(Force * (m_CurrentOrientation * Vector3.right) * Time.deltaTime * -CurrentVelocity.x * counterMovement);
            }

            if (Math.Abs(CurrentVelocity.y) > threshold && Math.Abs(m_MovementDirection.y) < 0.05f ||
                (CurrentVelocity.y < -threshold && m_MovementDirection.y > 0) ||
                (CurrentVelocity.y > threshold && m_MovementDirection.y < 0))
            {
                m_RigidBody.AddForce(Force * (m_CurrentOrientation * Vector3.forward) * Time.deltaTime * -CurrentVelocity.y * counterMovement);
            }

            if (Mathf.Sqrt((Mathf.Pow(m_RigidBody.velocity.x, 2) + Mathf.Pow(m_RigidBody.velocity.z, 2))) > MaxSpeed)
            {
                float fallspeed = m_RigidBody.velocity.y;
                Vector3 n = m_RigidBody.velocity.normalized * MaxSpeed;
                m_RigidBody.velocity = new Vector3(n.x, fallspeed, n.z);
            }
        }

        if (m_MovementDirection.x > 0 && CurrentVelocity.x > MaxSpeed ||
            m_MovementDirection.x < 0 && CurrentVelocity.x < -MaxSpeed)
        {
            m_MovementDirection.x = 0;
        }

        if (m_MovementDirection.y > 0 && CurrentVelocity.y > MaxSpeed ||
            m_MovementDirection.y < 0 && CurrentVelocity.y < -MaxSpeed)
        {
            m_MovementDirection.y = 0;
        }

        m_RigidBody.AddForce(Vector3.down * Gravity * Time.deltaTime);
        m_RigidBody.AddForce((m_CurrentOrientation * Vector3.forward) * m_MovementDirection.y * Force * Time.deltaTime);
        m_RigidBody.AddForce((m_CurrentOrientation * Vector3.right) * m_MovementDirection.x * Force * Time.deltaTime);
    }
    public Vector2 FindRelativeVelocity()
    {
        Vector2 Result = new Vector2();

        float Speed = m_RigidBody.velocity.magnitude;
        float MoveAngle = Mathf.Atan2(m_RigidBody.velocity.x, m_RigidBody.velocity.z) * Mathf.Rad2Deg;
        float Orientation = m_CurrentOrientation.y;

        float u = Mathf.DeltaAngle(Orientation, MoveAngle);
        float v = 90.0f - u;

        Result.x = Mathf.Cos(u * Mathf.Deg2Rad);
        Result.y = Mathf.Cos(v * Mathf.Deg2Rad);
        Result *= Speed;

        return Result;
    }

    bool IsFloor(Vector3 v)
    {
        float angle = Vector3.Angle(Vector3.up, v);
        return angle < maxSlopeAngle;
    }

    bool cancellingGrounded;

    void OnCollisionStay(Collision other)
    {
        int Layer = other.gameObject.layer;
        if (Solid != (Solid | (1 << Layer)))
        {
            return;
        }

        for (int i = 0; i < other.contactCount; i++)
        {
            Vector3 normal = other.contacts[i].normal;
            if (IsFloor(normal))
            {
                grounded = true;
                cancellingGrounded = false;
                normalVector = normal;
                CancelInvoke(nameof(StopGrounded));
            }
        }

        //Invoke ground/wall cancel, since we can't check normals with CollisionExit
        float delay = 3f;
        if (!cancellingGrounded)
        {
            cancellingGrounded = true;
            Invoke(nameof(StopGrounded), Time.deltaTime * delay);
        }
    }

    private void StopGrounded()
    {
        grounded = false;
    }
}
