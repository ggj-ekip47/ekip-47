﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using TMPro;

public class nesneEtkilesim : MonoBehaviour
{
    //[SerializeField] GameObject mCamera;
    bool tasiniyor;
    [SerializeField] Transform tasinanNesne;
    public float distance;
    public Transform holder;

    private bool _throw;
    //public GameObject nokta;

    Ray ray;
    RaycastHit hit;

    IEnumerator Throw()
    {
        _throw = true;
        yield return new WaitForSeconds(1f);
        _throw = false;
    }
    void Update()
    {
        if (tasinanNesne)
        {
            if (Input.GetMouseButtonDown(1))
            {
                StartCoroutine(Throw());
                tasiniyor = false;
                tasinanNesne.SetParent(null);
                tasinanNesne.GetComponent<Rigidbody>().isKinematic = false;
                tasinanNesne.GetComponent<Rigidbody>().AddForce(transform.forward * 15, ForceMode.Impulse);
                tasinanNesne = null;
            }
        }
        int x = Screen.width / 2;
        int y = Screen.height / 2;
        ray = Camera.main.ScreenPointToRay(new Vector3(x, y));
        if (Physics.Raycast(ray, out hit, distance))
        {
            // grab
            if (Input.GetMouseButton(0) && !_throw)
            {
                if ((hit.transform.GetComponent<CollectableBase>() || hit.transform.GetComponent<OVRGrabbable>())
                    && !tasiniyor)
                {
                    tasinanNesne = hit.transform;
                    tasinanNesne.GetComponent<Rigidbody>().isKinematic = true;
                    tasinanNesne.SetParent(holder);
                    //tasinanNesne.localPosition = Vector3.Lerp(tasinanNesne.localPosition, Vector3.zero, Time.deltaTime);
                    //tasinanNesne.localRotation = Quaternion.Lerp(tasinanNesne.localRotation, Quaternion.Euler(Vector3.zero), Time.deltaTime);
                    StartCoroutine(LerpObject());
                    tasinanNesne.GetComponent<IActions>()?.GrabAction(null);
                    tasiniyor = true;
                }
            }

            // click
            if (Input.GetMouseButton(1) && !tasiniyor)
            {
                hit.transform.GetComponent<IActions>()?.ClickAction(null);
            }
        }
        if (Input.GetMouseButtonUp(0) && tasinanNesne != null)
        {
            tasiniyor = false;
            tasinanNesne.SetParent(null);
            tasinanNesne.GetComponent<Rigidbody>().isKinematic = false;
            tasinanNesne.GetComponent<IActions>()?.ThrowAction(null);
            tasinanNesne = null;
        }
    }

    IEnumerator LerpObject()
    {
        while (true)
        {
            if (tasinanNesne != null)
            {
                tasinanNesne.localPosition = Vector3.Lerp(tasinanNesne.localPosition, Vector3.zero, Time.deltaTime * 10);
                tasinanNesne.localRotation = Quaternion.Lerp(tasinanNesne.localRotation, Quaternion.Euler(tasinanNesne.transform.forward), Time.deltaTime * 10);
            }

            yield return new WaitForEndOfFrame();
        }
    }
}
