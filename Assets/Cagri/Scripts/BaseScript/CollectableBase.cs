﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(Collider), typeof(Rigidbody))]
public class CollectableBase : MonoBehaviour
{
    private Rigidbody _rigidbody;
    public Rigidbody Rigidbody => (_rigidbody == null) ? _rigidbody = GetComponent<Rigidbody>() : _rigidbody;

    private Collider _collider;
    public Collider Collider => (_collider == null) ? _collider = GetComponent<Collider>() : _collider;

    private Transform _player;

    public enum CollectableType
    {
        RealItem,  // real "collectable"
        Trash
    };
    public CollectableType Type { get; protected set; }

    public CollectableBase()
    {
    }

    //protected virtual void OnTriggerEnter(Collider other)
    //{
    //    var comp = other.GetComponent<Cube>();
    //    if (comp)
    //    {
    //        GameManager.manager.collectedList.Add(gameObject);
    //        _player = comp;
    //    }
    //}
    /*
    public void Boom()
    {
        //GameManager.manager.counter = 0;
        //GameManager.manager.Player.GetComponent<Collider>().isTrigger = false;
        foreach (var collectable in GameManager.manager.collectedList)
        {
            collectable.GetComponent<Rigidbody>().isKinematic = false;
            collectable.GetComponent<Collider>().enabled = true;
        }
        StartCoroutine(BoomCollectable());
    }
    */    
    /*
    private float blastRadius = 35f;
    IEnumerator BoomCollectable()
    {
        var timer = 0f;
        while (true)
        {
            Collider[] colliders = Physics.OverlapSphere(GameManager.manager.suitcase.position, blastRadius);
            foreach (Collider nearbyObject in colliders)
            {
                Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
                if (rb != GameManager.manager.Player.GetComponent<Rigidbody>())
                {
                    rb.AddExplosionForce(25, transform.position, blastRadius, 1f, ForceMode.Impulse);
                    yield return new WaitForEndOfFrame();
                }
            }
            break;
        }
        yield return new WaitForSeconds(.75f);
        GameManager.manager.Player.GetComponent<Collider>().isTrigger = true;
    }*/

}
