﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IActions
{ 
    void ThrowAction(object Data);
    void GrabAction(object Data);
    void ClickAction(object Data);
}
