﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LambActions : MonoBehaviour, IActions
{
    public Light LightObject;

    public void ClickAction(object Data)
    {
        LightObject.enabled = !LightObject.enabled;
    }

    public void GrabAction(object Data)
    {
        LightObject.enabled = true;
    }

    public void ThrowAction(object Data)
    {
        LightObject.enabled = false;
    }
}
