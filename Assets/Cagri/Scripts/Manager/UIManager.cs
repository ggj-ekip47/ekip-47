﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager manager;

    public GameObject first;
    public GameObject second;
    public GameObject third;
    public GameObject fourth;
    public GameObject fifth;
    public GameObject winGame;

    private void Awake()
    {
        manager = this;
    }

    private void Update()
    {
        switch (GameManager.manager.TargetPriority)
        {
            case CollectablePriority.rankOfPriority.First:
                {
                    first.SetActive(true);
                    second.SetActive(false);
                    third.SetActive(false);
                    fourth.SetActive(false);
                    fifth.SetActive(false);
                    break;
                }
            case CollectablePriority.rankOfPriority.Second:
                {
                    first.SetActive(false);
                    second.SetActive(true);
                    third.SetActive(false);
                    fourth.SetActive(false);
                    fifth.SetActive(false);
                    break;
                }
            case CollectablePriority.rankOfPriority.Third:
                {
                    first.SetActive(false);
                    second.SetActive(false);
                    third.SetActive(true);
                    fourth.SetActive(false);
                    fifth.SetActive(false);
                    break;
                }
            case CollectablePriority.rankOfPriority.Fourth:
                {
                    first.SetActive(false);
                    second.SetActive(false);
                    third.SetActive(false);
                    fourth.SetActive(true);
                    fifth.SetActive(false);
                    break;
                }
            case CollectablePriority.rankOfPriority.Fifth:
                {
                    first.SetActive(false);
                    second.SetActive(false);
                    third.SetActive(false);
                    fourth.SetActive(false);
                    fifth.SetActive(true);
                    break;
                }
            case CollectablePriority.rankOfPriority.DONE:
                {
                    first.SetActive(false);
                    second.SetActive(false);
                    third.SetActive(false);
                    fourth.SetActive(false);
                    fifth.SetActive(false);
                    winGame.SetActive(true);
                    break;
                }
        }
    }
}
