﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collector : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "PlayerController" ||
            other.gameObject.name == "GrabManager")
        {
            return;
        }

        if (!GameManager.manager.CollectItem(other.gameObject))
        {
            Boom();
        }
    }

    public void Boom()
    {
        //GameManager.manager.counter = 0;
        //GameManager.manager.Player.GetComponent<Collider>().isTrigger = false;
        foreach (var collectable in GameManager.manager.collectedList)
        {
            collectable.GetComponent<Rigidbody>().isKinematic = false;
            collectable.GetComponent<Collider>().enabled = true;
        }
        StartCoroutine(BoomCollectable());
    }
    private float blastRadius = 35.0f;
    private float blastForce = 1.0f;
    IEnumerator BoomCollectable()
    {
        var timer = 0f;
        while (true)
        {
            Collider[] colliders = Physics.OverlapSphere(GameManager.manager.suitcase.position, blastRadius);
            foreach (Collider nearbyObject in colliders)
            {
                Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
                if(rb!=null)
                {
                    rb.AddExplosionForce(blastForce, transform.position, blastRadius, 1f, ForceMode.Impulse);
                }
                
                yield return new WaitForEndOfFrame();

            }
            break;
        }
        yield return new WaitForSeconds(.75f);
        //GameManager.manager.Player.GetComponent<Collider>().isTrigger = true;
    }
}
