﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectablePriority : CollectableBase
{
    public enum rankOfPriority
    {
        First = 0,
        Second,
        Third,
        Fourth,
        Fifth,
        DONE
    }

    public rankOfPriority priority;

    public CollectablePriority()
    {
        Type = CollectableType.RealItem;
    }

    // todo picture or collectable symbol

    //protected override void OnTriggerEnter(Collider other)
    public void PutInBox()
    {
        //base.OnTriggerEnter(other);
        Transform Target = null;

        switch (priority)
        {
            case rankOfPriority.First:
                {
                    Target = GameManager.manager.first;
                    break;
                }
            case rankOfPriority.Second:
                {
                    Target = GameManager.manager.second;
                    break;
                }

            case rankOfPriority.Third:
                {
                    Target = GameManager.manager.third;
                    break;
                }

            case rankOfPriority.Fourth:
                {
                    Target = GameManager.manager.fourth;
                    break;
                }
            case rankOfPriority.Fifth:
                {
                    Target = GameManager.manager.fifth;
                    break;
                }
        }

        if (Target != null)
        {
            transform.SetParent(Target);
            StartCoroutine(TransformObject());
        }

        //switch (priority)
        //{
        //    case rankOfPriority.First:
        //        if (GameManager.manager.counter == 0)
        //        {
        //            //GameManager.manager.counter++;
        //            transform.SetParent(GameManager.manager.first);
        //            StartCoroutine(TransformObject());
        //        }
        //        else
        //        {
        //            Boom();
        //        }
        //        break;
        //    case rankOfPriority.Second:
        //        if (GameManager.manager.counter == 1)
        //        {
        //            //GameManager.manager.counter++;
        //            transform.SetParent(GameManager.manager.second);
        //            StartCoroutine(TransformObject());
        //        }
        //        else
        //        {
        //            Boom();
        //        }
        //        break;
        //    case rankOfPriority.Third:
        //        if (GameManager.manager.counter == 2)
        //        {
        //            //GameManager.manager.counter++;
        //            transform.SetParent(GameManager.manager.third);
        //            StartCoroutine(TransformObject());
        //        }
        //        else
        //        {
        //            Boom();
        //        }
        //        break;
        //    case rankOfPriority.Fourth:
        //        if (GameManager.manager.counter == 3)
        //        {
        //            //GameManager.manager.counter++;
        //            transform.SetParent(GameManager.manager.fourth);
        //            StartCoroutine(TransformObject());
        //        }
        //        else
        //        {
        //            Boom();
        //        }
        //        break;
        //    case rankOfPriority.Fifth:
        //        if (GameManager.manager.counter == 4)
        //        {
        //            //GameManager.manager.counter++;
        //            transform.SetParent(GameManager.manager.fifth);
        //            StartCoroutine(TransformObject());
        //            //todo win 
        //        }
        //        else
        //        {
        //            Boom();
        //        }
        //        break;
        //    default:
        //        throw new ArgumentOutOfRangeException();
        //}
    }

    IEnumerator TransformObject()
    {
        var timer = 0f;
        while (true)
        {
            transform.GetComponent<Collider>().enabled = false;
            transform.GetComponent<Rigidbody>().isKinematic = true;
            timer += Time.deltaTime;
            transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, timer * .15f);
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(Vector3.zero), timer * .15f);
            if (timer >= 1f)
            {
                break;
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
