﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class yuru : MonoBehaviour
{
    
        [SerializeField] CharacterController controller;
        [SerializeField] float speed = 5f;
        //float movX, movY;
        //Vector3 move;
        Vector3 velocity;
        float gravity = -9.81f;

        

        void Update()
        {
            //move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            //controller.Move(move * Time.deltaTime * 5);

            if (controller.isGrounded && velocity.y < 0)
            {
                velocity.y = -2f;
            }

            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");
            Vector3 move = transform.right * x + transform.forward * z;
            controller.Move(move * speed * Time.deltaTime);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                velocity.y = 5;
            }

            velocity.y += gravity * Time.deltaTime;
            controller.Move(velocity * Time.deltaTime);
        }
    

}
